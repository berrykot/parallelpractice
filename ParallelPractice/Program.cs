﻿using System;
using System.Diagnostics;

namespace ParallelPractice
{
	class Program
	{
		static void Main(string[] args)
		{
			var intArray = new IntArray(new IntArrayGenerator().GenerateIntArray(500_000_000));
			ShowExecutionDuration(intArray.SumCommon);
			ShowExecutionDuration(intArray.SumParallel);
			ShowExecutionDuration(intArray.SumLinq);
		}

		private static void ShowExecutionDuration(Action action)
		{
			if (action is null)
				throw new ArgumentNullException(nameof(action));

			var sw = new Stopwatch();
			sw.Start();
			action();
			sw.Stop();
			Console.WriteLine($"Процесс выполнился за {sw.ElapsedMilliseconds} милисекунд");
		}
	}
}
