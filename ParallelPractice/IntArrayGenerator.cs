﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ParallelPractice
{
	class IntArrayGenerator
	{
		private Random _rnd;

		public IntArrayGenerator()
		{
			_rnd = new Random();
		}

		public int[] GenerateIntArray(uint size)
		{
			var array = new int[size];
			for (int i = 0; i < size; i++)
				array[i] = _rnd.Next(10, 99);
			return array;
		}
	}
}
