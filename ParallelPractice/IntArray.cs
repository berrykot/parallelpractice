﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelPractice
{
	class IntArray
	{
		private int[] _array;
		private object _locker;
		public long Sum { get; set; }
		public IntArray(int[] array)
		{
			_locker = new object();
			_array = array;
		}
		public void SumCommon()
		{
			Sum = 0;
			for (int i = 0; i < _array.Length; i++)
				Sum += _array[i];

			Console.WriteLine(Sum);
		}
		public void SumParallel()
		{
			Sum = 0;
			const int threadsNumber = 5;
			var threadPool = new List<Thread>(threadsNumber);
			var itemsInThread = _array.Length / threadsNumber;
			for (var number = 0; number < threadsNumber; number++)
			{
				var thread = new Thread(new ParameterizedThreadStart( state =>
					{
						var sumInThread = 0;
						var firstIndex = (int)state * itemsInThread;
						var lastIndex = firstIndex + itemsInThread;
						for (int i = firstIndex; i < lastIndex; i++)
							sumInThread += _array[i];

						lock (_locker)
							Sum += sumInThread;
					}));
				threadPool.Add(thread);
				thread.Start(number);
			}
			threadPool.ForEach(t => t.Join());
			Console.WriteLine(Sum);
		}
		public void SumThreadPool()
		{
			Sum = 0;
			const int threadsNumber = 5;
			int itemsInThread = _array.Length / threadsNumber;
			for (int number = 0; number < threadsNumber; number ++)
			{
				ThreadPool.QueueUserWorkItem(state =>
				{
					var sumInThread = 0;
					var firstIndex = (int)state * itemsInThread;
					var lastIndex = firstIndex + itemsInThread;
					for (int i = firstIndex; i < lastIndex; i++)
						sumInThread += _array[i];

					lock (_locker)
						Sum += sumInThread;
				}, number);
			}

			Console.WriteLine(Sum);
		}
		public void SumParallelFor()
		{
			Sum = 0;
			const int threadsNumber = 5;
			int itemsInThread = _array.Length / threadsNumber;

			Parallel.For(0, threadsNumber, number =>
			{
				var sumInThread = 0;
				var firstIndex = number * itemsInThread;
				var lastIndex = firstIndex + itemsInThread;
				for (int i = firstIndex; i < lastIndex; i++)
					sumInThread += _array[i];

				lock (_locker)
					Sum += sumInThread;
			});

			Console.WriteLine(Sum);
		}
		public void SumLinq()
		{
			Sum = 0;
			const int threadsNumber = 5;
			int itemsInThread = _array.Length / threadsNumber;
			var arrayThreads = new int[threadsNumber];
			for (int i = 0; i < threadsNumber; i++)
				arrayThreads[i] = itemsInThread * i;

			arrayThreads.AsParallel().ForAll(firstIndex =>
			{
				var sumInThread = 0;
				var lastIndex = firstIndex + itemsInThread;
				for (int i = firstIndex; i < lastIndex; i++)
					sumInThread += _array[i];

				lock (_locker)
					Sum += sumInThread;
			});

			Console.WriteLine(Sum);
		}
	}
}
